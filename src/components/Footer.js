import React from 'react';
import umbraco from '../api/umbraco';

class Footer extends React.Component {

	state = { leftText: "", rightText: "", email: "" };

	loadTextData = async (field) => {
		//quick validation
		if(field !== 'lefttext' && field !== 'righttext' && field !== 'email'){
			return;
		}

		//load the data
		const response = await umbraco.get('/QueryData/', {
			params: {
				page: 'homepage',
				section: 'footer',
				field: field,
			}
		});

		switch(field){
			case 'lefttext':
				this.setState({leftText: response.data.value});
				break;
			case 'righttext':
				this.setState({rightText: response.data.value});
				break;
			case 'email':
				this.setState({email: response.data.value});
				break;
			default:
				return;
		}
	};

	componentDidMount() {
		this.loadTextData('lefttext');
		this.loadTextData('righttext');
		this.loadTextData('email');
	}

	render(){

		if(this.state.leftText && this.state.rightText){
			return (
				<footer>
					<div dangerouslySetInnerHTML={{__html: (this.state.leftText + this.state.email)}}></div>
					<div dangerouslySetInnerHTML={{__html: this.state.rightText}}></div>
				</footer>
			);
		} else {
			return (
				<footer>
					<div>3</div>
					<div>4</div>
				</footer>
			);
		}
		
	}
}

export default Footer;
