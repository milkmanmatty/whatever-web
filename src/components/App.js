import React from 'react';
import Header from './Header';
import Main from './Main';
import Footer from './Footer';
import '../style/App.css';

class App extends React.Component {

	constructor(props){
		super(props)
		this.state = {
			x: 0,
			y: 0
		};
	}

	onMouseMove = (e) => {
		this.setState({
			x: (e.nativeEvent.offsetX / window.innerWidth),
			y: (e.nativeEvent.offsetY / window.innerHeight)
		});
	}

	render(){
		return (
			<div>
				<Header />
				<Main x={this.state.x} y={this.state.y} />
				<Footer />
				<div onMouseMove={this.onMouseMove}></div>
			</div>
		);
	}
}

export default App;
