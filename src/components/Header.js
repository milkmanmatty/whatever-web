import React from 'react';
import umbraco from '../api/umbraco';

class Header extends React.Component {

	state = { leftText: "", rightText: "" };

	loadTextData = async (direction) => {
		//quick validation
		if(direction !== 'left' && direction !== 'right'){
			return;
		}

		//load the data
		const response = await umbraco.get('/QueryData/', {
			params: {
				page: 'homepage',
				section: 'header',
				field: direction + 'text',
			}
		});

		if(direction === 'left'){
			this.setState({leftText: response.data.value});
		} else {
			this.setState({rightText: response.data.value});
		}
		return;
	};

	componentDidMount() {
		this.loadTextData('left');
		this.loadTextData('right');
	}

	render(){

		if(this.state.leftText && this.state.rightText){
			return (
				<header>
					<div dangerouslySetInnerHTML={{__html: this.state.leftText}}></div>
					<div dangerouslySetInnerHTML={{__html: this.state.rightText}}></div>
				</header>
			);
		} else {
			return (
				<header>
					<div>1</div>
					<div>2</div>
				</header>
			);
		}
		
	}
}

export default Header;
