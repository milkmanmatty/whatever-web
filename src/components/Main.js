import React from 'react';
import umbraco from '../api/umbraco';

class Main extends React.Component {

	state = { heading: "" };

	loadTextData = async (field) => {

		//load the data
		const response = await umbraco.get('/QueryData/', {
			params: {
				page: 'homepage',
				section: '',
				field: field,
			}
		});

		switch(field){
			case 'heading':
				this.setState({heading: response.data.value});
				break;
			default:
				return;
		}
	};

	componentDidMount() {
		this.loadTextData('heading');
	}

	componentDidUpdate(prevProps) {
		if((this.props.x !== prevProps.x) || (this.props.y !== prevProps.y)){
			this.render();
		}
	} 

	render(){

		if(this.state.heading){
			return (
				<main>
					<h1 
						dangerouslySetInnerHTML={{__html: this.state.heading}} 
						style={{fontVariationSettings: '"wght" '+(1000*this.props.y)+', "wdth" '+(1000*this.props.x),
								letterSpacing: (this.props.x*5)+'vw'
						}}
					></h1>
				</main>
			);
		} else {
			return (
				<main>
					<h1>Whatever</h1>
				</main>
			);
		}
		
	}
}

export default Main;
